var express = require('express');
var router = express.Router();

var conversation = [];

// Filter conversation messages and only return those before a given timestamp.
function getMessagesAfter(after) {	
	var newMessages = [];	
	for (var i=0; i<conversation.length; i++) {
		console.log("Comparing: " + conversation[i].timestamp + " > " + after);
		if (conversation[i].timestamp.getTime() > after) {			
			// Oldest first.
			newMessages.unshift(conversation[i]);
		} else {
			break;
		}
	}
	return newMessages;
}

router.get('/', function(req, res, next) {
	res.json({
		messages: getMessagesAfter(Date.parse(req.param('after'))),
		lastUpdate: new Date()
	});
});

router.post('/', function(req, res, next) {
  var message = {
      ip: req.ip,
      timestamp: new Date(),
      user: req.param('user'),
      text: req.param('text')
  }
  conversation.unshift(message);
  res.json(message);
});

module.exports = router;
