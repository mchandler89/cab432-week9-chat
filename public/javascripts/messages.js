$(document).ready(function() {
	bindMessageSubmit();
	loadMessagesAfter(0);
	poll();
});

var lastUpdate;

// Bind to submission of the messages form.
function bindMessageSubmit() {
	$('#message-form').submit(handleMessageSubmit);
}

function handleMessageSubmit(event) {
	event.preventDefault();
	$.post('/messages', $(event.target).serialize(), function(data) {
		// Keep it simple & do nothing.
		// Message will be shown on next poll.
	});
}

// Load messages after a given timestamp.
function loadMessagesAfter(timestamp) {
	$.get('/messages', {after: timestamp}, function(data) {
		showMessages(data.messages);
		lastUpdate = data.lastUpdate;
	});
}

// Recursively poll for new messages
function poll() {
	setTimeout(function(){ 
		loadMessagesAfter(lastUpdate);
		poll();
	}, 1000);
}

// Generate an HTML row for a given message.
function generateRow(message) {
	return "<tr>" +
			"<td>" + new Date(message.timestamp).toLocaleString() + "</td>" +
			"<td>" + message.user + "</td>" +
			"<td>" + message.text + "</td>" +
			"<td>" + message.ip + "</td>" +
		"</tr>";
}

// Show a single message on the screen.
function showMessage(message) {
	$('#messages-container tbody').prepend(generateRow(message));
}

// Show multiple messages on the screen.
// Assumes messages are sorted OLDEST FIRST.
function showMessages(messages) {
	$(messages).each(function() {
		showMessage(this);
	});
}